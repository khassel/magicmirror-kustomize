# **MagicMirror²**

is an open source modular smart mirror platform. For more info visit the [project website](https://github.com/MichMich/MagicMirror).

# Introduction

This is a kustomize setup for MagicMirror.

> 👉 This is a fun project testing a translation of the existing [`helm` setup](https://gitlab.com/khassel/magicmirror-helm) to `kustomize`. A `helm` setup is more flexible but often over complicated. So for me as a `kustomize` beginner there were some challenges to solve and this project has more of an educational purpose.

# Restrictions

A normal MagicMirror setup uses a raspberry pi as hardware. If you want to run the application without output on the screen of the pi, you can run it [server only](https://docs.magicmirror.builders/getting-started/installation.html#usage) and open MagicMirror with a web browser.

As the most k8s clusters are not running on raspberry pi's, here the `server only` setup of MagicMirror is used.

> ❌ This implies that no modules can be used which need raspberry pi specific hardware (e.g. GPIO).

# Prerequisites

* Running Kubernetes Cluster
* kustomize >= version `v4.5.3` [(because of this issue)](https://github.com/kubernetes-sigs/kustomize/issues/4386)

# Install

To install MagicMirror

```bash
$ git clone https://gitlab.com/khassel/magicmirror-kustomize.git
```

For running MagicMirror with the default setup

```bash
$ cd magicmirror-kustomize/base
$ kustomize build . | kubectl apply -f -
```

For running MagicMirror with some foreign modules

```bash
$ cd magicmirror-kustomize/overlays/example
$ kustomize build . | kubectl apply -f -
```

# Uninstalling

To uninstall/delete the deployment go to the folder from where you ran the install command and run `kustomize build . | kubectl delete -f -`

# Configuration

You can create your own overlay, see the example in `overlays/example`.
